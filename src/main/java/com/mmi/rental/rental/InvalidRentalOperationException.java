package com.mmi.rental.rental;

public class InvalidRentalOperationException extends RuntimeException {
    public InvalidRentalOperationException(String message) {
        super(message);
    }
}
