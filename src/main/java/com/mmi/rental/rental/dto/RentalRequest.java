package com.mmi.rental.rental.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

import java.util.List;

@Data
public class RentalRequest {

    @Valid
    private List<RentalRequestItem> items;

    @NotNull
    @Positive
    private Integer numberOfDays;

    @Data
    public static class RentalRequestItem {//Need prefix due to swagger not properly distinguishing classes with same name

        @NotBlank
        private String movieId;
        @NotNull
        @Positive
        private Integer quantity;
    }
}

