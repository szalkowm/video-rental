package com.mmi.rental.rental.dto;

import com.mmi.rental.inventory.Movie;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Quote {
    private Movie movie;
    private BigDecimal upfrontFee;
    private Integer noOfDays;
    private Integer quantity;
}
