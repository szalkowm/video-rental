package com.mmi.rental.rental;

import com.mmi.rental.application.persistence.InMemoryRepository;
import com.mmi.rental.application.persistence.Repository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class RentalConfiguration {


    @Bean
    Repository<Rental, String> rentalRepository() {
        Repository<Rental, String> rentalRepository = new InMemoryRepository<>() {
            @Override
            protected String genereateId() {
                return UUID.randomUUID().toString();
            }
        };
        return rentalRepository;
    }
}
