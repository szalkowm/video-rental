package com.mmi.rental.rental;

import com.mmi.rental.inventory.MovieType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "rental")
public class RentalProperties {

    private List<Price> prices;

    @Data
    public static class Price {
        private MovieType type;
        private int threshold;
        private BigDecimal flatPrice;
        private BigDecimal dailyPrice;
    }
}
