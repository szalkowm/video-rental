package com.mmi.rental.rental;

import com.mmi.rental.application.persistence.Identifiable;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.TimeZone;

@Data
public class Rental implements Identifiable<String> {
    private String id;
    private List<Item> items;
    private Instant rentedAt;
    private Instant returnedAt;
    private Status status = Status.ACTIVE;

    public void returnRental(Instant returnTime) {
        status = Status.RETURNED;
        returnedAt = returnTime;
    }

    public int daysRented() {
        if (status != Status.RETURNED) {
            throw new IllegalStateException("Only returned rentals can calculate duration");
        }
        LocalDate start = LocalDate.ofInstant(rentedAt, TimeZone.getDefault().toZoneId());
        LocalDate end = LocalDate.ofInstant(returnedAt, TimeZone.getDefault().toZoneId());
        return Math.toIntExact(end.toEpochDay() - start.toEpochDay());
    }

    @Data
    public static class Item {
        private String movieId;
        private Integer quantity;
        private BigDecimal upfrontFee;
        private BigDecimal lateFee;
    }

    public enum Status {
        ACTIVE,
        RETURNED,
    }
}
