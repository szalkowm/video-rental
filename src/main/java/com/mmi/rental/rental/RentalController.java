package com.mmi.rental.rental;

import com.mmi.rental.application.dto.Error;
import com.mmi.rental.application.persistence.Repository;
import com.mmi.rental.rental.dto.Quote;
import com.mmi.rental.rental.dto.RentalRequest;
import com.mmi.rental.rental.usecase.CreateRental;
import com.mmi.rental.rental.usecase.QuoteRental;
import com.mmi.rental.rental.usecase.ReturnRental;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/rentals")
public class RentalController {

    private final QuoteRental quoteRental;
    private final CreateRental createRental;
    private final ReturnRental returnRental;
    private final Repository<Rental, String> rentalRepository;


    @GetMapping
    public List<Rental> allRentals() {
        return rentalRepository.findAll();
    }

    @PostMapping("/quote")
    @Operation(summary = "Receive pricing of a rental without persisting it")
    public List<Quote> quoteRental(@Valid @RequestBody RentalRequest request) {
        return quoteRental.quoteItem(request);
    }

    @PostMapping
    @Operation(summary = "Create a rental")
    public Rental rent(@Valid @RequestBody RentalRequest request) {
        return createRental.createRental(request);
    }

    @ExceptionHandler({InvalidRentalOperationException.class})
    ResponseEntity<Error> handleInvalidOperation(InvalidRentalOperationException exception) {
        Error error = new Error();
        error.setMessage(exception.getMessage());
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

    @PutMapping("/{id}/return")
    @Operation(summary = "Return a rental")
    public Rental returnRental(@PathVariable String id) {
        return returnRental.returnRental(id);
    }
}

