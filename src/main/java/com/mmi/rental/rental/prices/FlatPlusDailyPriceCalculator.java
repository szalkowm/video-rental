package com.mmi.rental.rental.prices;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;


@RequiredArgsConstructor
public class FlatPlusDailyPriceCalculator implements PriceCalculator {

    private final int threshold;
    private final BigDecimal flatPrice;
    private final BigDecimal dailyPrice;

    @Override
    public BigDecimal calculatePrice(int noOfDays, int quantity) {
        int daysCharged = Math.max(0, noOfDays - threshold);
        BigDecimal singleItemPrice = flatPrice.add(dailyPrice.multiply(new BigDecimal(daysCharged)));
        return singleItemPrice.multiply(new BigDecimal(quantity));
    }
}
