package com.mmi.rental.rental.prices;

import java.math.BigDecimal;

public interface PriceCalculator {

    BigDecimal calculatePrice(int noOfDays, int quantity);
}


