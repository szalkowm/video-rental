package com.mmi.rental.rental.prices;

import com.mmi.rental.inventory.Movie;
import com.mmi.rental.inventory.MovieType;
import com.mmi.rental.rental.RentalProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Component
public class PriceCalculatorFactory {
    private final Map<MovieType, PriceCalculator> calculators;

    public PriceCalculatorFactory(RentalProperties properties) {
        this.calculators = properties.getPrices().stream().collect(toMap(
                RentalProperties.Price::getType,
                price -> new FlatPlusDailyPriceCalculator(price.getThreshold(), price.getFlatPrice(), price.getDailyPrice())
        ));
        validateAllPresent();
    }

    private void validateAllPresent() {
        for (MovieType type : MovieType.values()) {
            if (!calculators.containsKey(type)) {
                throw new IllegalStateException("Missing price config for type:" + type);
            }
        }
    }


    public PriceCalculator getCalculator(Movie movie) {
        return calculators.get(movie.getType());
    }
}
