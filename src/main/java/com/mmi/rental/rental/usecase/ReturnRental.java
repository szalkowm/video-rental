package com.mmi.rental.rental.usecase;

import com.mmi.rental.application.persistence.Repository;
import com.mmi.rental.inventory.Movie;
import com.mmi.rental.rental.InvalidRentalOperationException;
import com.mmi.rental.rental.Rental;
import com.mmi.rental.rental.prices.PriceCalculator;
import com.mmi.rental.rental.prices.PriceCalculatorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Clock;
import java.util.function.Supplier;

import static java.time.Instant.now;


@Component
@RequiredArgsConstructor
public class ReturnRental {

    private final Repository<Rental, String> rentalRepository;
    private final Repository<Movie, String> moviesRepository;
    private final PriceCalculatorFactory calculatorFactory;
    private final Supplier<Clock> clockSupplier;


    public Rental returnRental(String id) {
        Rental rental = findRental(id);
        rental.returnRental(now(clockSupplier.get()));
        assignLateFees(rental);
        rentalRepository.save(rental);
        return rental;
    }

    private Rental findRental(String id) {
        Rental rental = rentalRepository.findById(id);
        if (rental.getStatus() != Rental.Status.ACTIVE) {
            throw new InvalidRentalOperationException("Can only return an active rental");
        }
        return rental;
    }

    private void assignLateFees(Rental rental) {

        int daysRented = rental.daysRented();

        for (Rental.Item item : rental.getItems()) {
            PriceCalculator calculator = calculatorFactory.getCalculator(moviesRepository.findById(item.getMovieId()));
            BigDecimal totalCost = calculator.calculatePrice(daysRented, item.getQuantity());
            BigDecimal lateFee = totalCost.subtract(item.getUpfrontFee());
            lateFee = lateFee.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : lateFee;
            item.setLateFee(lateFee);
        }
    }

}
