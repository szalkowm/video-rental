package com.mmi.rental.rental.usecase;

import com.mmi.rental.application.persistence.Repository;
import com.mmi.rental.inventory.Movie;
import com.mmi.rental.rental.InvalidRentalOperationException;
import com.mmi.rental.rental.Rental;
import com.mmi.rental.rental.dto.RentalRequest;
import com.mmi.rental.rental.prices.PriceCalculator;
import com.mmi.rental.rental.prices.PriceCalculatorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingInt;


@Component
@RequiredArgsConstructor
public class CreateRental {

    private final Repository<Rental, String> rentalRepository;
    private final Repository<Movie, String> moviesRepository;
    private final PriceCalculatorFactory calculatorFactory;

    private final Supplier<Clock> clockSupplier;


    public Rental createRental(RentalRequest request) {
        checkAvailability(request);
        Rental rental = new Rental();
        rental.setItems(buildItemsList(request));
        rental.setRentedAt(Instant.now(clockSupplier.get()));
        rentalRepository.save(rental);
        return rental;
    }

    private void checkAvailability(RentalRequest request) {
        Map<String, Integer> rentedCopies = buildRentedCopiesMap();
        for (RentalRequest.RentalRequestItem item : request.getItems()) {
            checkAvailability(item, rentedCopies);
        }
    }

    private void checkAvailability(RentalRequest.RentalRequestItem item, Map<String, Integer> rentedCopies) {
        Movie movie = moviesRepository.findById(item.getMovieId());
        int rented = rentedCopies.getOrDefault(item.getMovieId(), 0);
        int availableCopies = movie.getQuantity() - rented;
        if (availableCopies - item.getQuantity() < 0) {
            throw new InvalidRentalOperationException("Not enough available copies of movie:" + movie.getId());
        }
    }

    private Map<String, Integer> buildRentedCopiesMap() {
        return rentalRepository.findAll().stream()
                .filter(rental -> rental.getStatus() == Rental.Status.ACTIVE)
                .map(Rental::getItems).flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Rental.Item::getMovieId, summingInt(Rental.Item::getQuantity)));
    }

    private List<Rental.Item> buildItemsList(RentalRequest request) {
        List<Rental.Item> rentalItems = new ArrayList<>();
        for (RentalRequest.RentalRequestItem item : request.getItems()) {
            rentalItems.add(mapRentalItem(item, request.getNumberOfDays()));
        }
        return rentalItems;
    }

    private Rental.Item mapRentalItem(RentalRequest.RentalRequestItem item, int noOfDays) {
        Rental.Item rentalItem = new Rental.Item();
        rentalItem.setQuantity(item.getQuantity());
        rentalItem.setMovieId(item.getMovieId());
        PriceCalculator calculator = calculatorFactory.getCalculator(moviesRepository.findById(item.getMovieId()));
        rentalItem.setUpfrontFee(calculator.calculatePrice(noOfDays, item.getQuantity()));
        return rentalItem;
    }
}
