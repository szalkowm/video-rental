package com.mmi.rental.rental.usecase;

import com.mmi.rental.application.persistence.Repository;
import com.mmi.rental.inventory.Movie;
import com.mmi.rental.rental.dto.Quote;
import com.mmi.rental.rental.dto.RentalRequest;
import com.mmi.rental.rental.prices.PriceCalculator;
import com.mmi.rental.rental.prices.PriceCalculatorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
@RequiredArgsConstructor
public class QuoteRental {

    private final Repository<Movie, String> moviesRepository;
    private final PriceCalculatorFactory calculatorFactory;

    public List<Quote> quoteItem(RentalRequest request) {
        return request.getItems().stream().map(item -> quoteItem(item, request.getNumberOfDays())).toList();
    }

    private Quote quoteItem(RentalRequest.RentalRequestItem item, int numberOfDays) {
        Movie movie = moviesRepository.findById(item.getMovieId());
        PriceCalculator calculator = calculatorFactory.getCalculator(movie);
        Quote quote = new Quote();
        quote.setMovie(movie);
        quote.setQuantity(item.getQuantity());
        quote.setNoOfDays(numberOfDays);
        BigDecimal fee = calculator.calculatePrice(numberOfDays, item.getQuantity());
        BigDecimal totalFee = fee.multiply(new BigDecimal(item.getQuantity()));
        quote.setUpfrontFee(totalFee);
        return quote;
    }
}
