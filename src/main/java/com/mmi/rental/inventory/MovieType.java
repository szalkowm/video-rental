package com.mmi.rental.inventory;

public enum MovieType {
    NEW_RELEASE,
    OLD,
    REGULAR
}
