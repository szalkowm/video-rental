package com.mmi.rental.inventory;


import com.mmi.rental.application.persistence.Repository;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/movies")
public class MovieController {

    private final Repository<Movie, String> moviesRepository;

    @GetMapping
    @Operation(summary = "Fetch list of available Movies")
    Iterable<Movie> allMovies() {
        return moviesRepository.findAll();
    }

    @PostMapping
    Movie addMovie(@RequestBody @Valid Movie movie) {
        moviesRepository.save(movie);
        return movie;
    }

    @PutMapping
    Movie updateMovie(@RequestBody @Valid Movie movie) {
        moviesRepository.findById(movie.getId());
        moviesRepository.save(movie);
        return movie;
    }
}
