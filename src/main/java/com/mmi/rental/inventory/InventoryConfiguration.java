package com.mmi.rental.inventory;

import com.mmi.rental.application.persistence.InMemoryRepository;
import com.mmi.rental.application.persistence.Repository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.UUID;

@Configuration
public class InventoryConfiguration {


    @Bean
    Repository<Movie, String> moviesRepository() {
        Repository<Movie, String> moviesRepository = new InMemoryRepository<>() {
            @Override
            protected String genereateId() {
                return UUID.randomUUID().toString();
            }
        };
        mockData().forEach(moviesRepository::save);
        return moviesRepository;
    }

    private List<Movie> mockData() {
        Movie matrix = new Movie();
        matrix.setType(MovieType.NEW_RELEASE);
        matrix.setTitle("Matrix 11");
        matrix.setId("matrix11");

        Movie spiderMan = new Movie();
        spiderMan.setType(MovieType.REGULAR);
        spiderMan.setTitle("Spider Man");
        spiderMan.setId("spiderMan");

        Movie spiderMan2 = new Movie();
        spiderMan2.setType(MovieType.REGULAR);
        spiderMan2.setTitle("Spider Man 2");
        spiderMan2.setQuantity(2);
        spiderMan2.setId("spiderMan2");

        Movie ooAfrica = new Movie();
        ooAfrica.setType(MovieType.OLD);
        ooAfrica.setTitle("Out of Africa");
        ooAfrica.setId("ooAfrica");

        return List.of(matrix, spiderMan, spiderMan2, ooAfrica);
    }
}
