package com.mmi.rental.inventory;

import com.mmi.rental.application.persistence.Identifiable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;


@Data
public class Movie implements Identifiable<String> {

    private String id;
    @NotNull
    private MovieType type;

    @NotNull
    @NotBlank
    private String title;

    @Positive
    @NotNull
    private Integer quantity = 1;
}
