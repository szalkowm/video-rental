package com.mmi.rental.application;


import com.mmi.rental.application.dto.ClockOffsetRequest;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RequiredArgsConstructor
@RestController
@RequestMapping("debug/v1/clock")
public class ClockController {

    private final ClockSupplier clockSupplier;

    @PutMapping
    @Operation(summary = "Offsets application clock by specified time. All following actions will use the offset time" +
            " for calculations. Overwrites previously set offsets.")
    public void offsetClock(@RequestBody @Valid ClockOffsetRequest request) {
        clockSupplier.offsetByHours(request.getOffsetHours());
    }

    @DeleteMapping
    @Operation(summary = "Removes all offsets")
    public void reset() {
        clockSupplier.reset();
    }
}
