package com.mmi.rental.application.persistence;

public interface Identifiable<ID> {

    void setId(ID id);

    ID getId();

}
