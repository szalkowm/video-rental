package com.mmi.rental.application.persistence;

import java.util.List;

public interface Repository<T extends Identifiable<ID>, ID> {
    void save(T entity);

    void delete(T entity);

    T findById(ID id);

    List<T> findAll();

    List<T> findAllByIds(List<ID> ids);
}
