package com.mmi.rental.application.persistence;

import com.mmi.rental.application.exceptions.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

public abstract class InMemoryRepository<T extends Identifiable<ID>, ID> implements Repository<T, ID> {

    private final Map<ID, T> entities = new ConcurrentHashMap<>();

    @Override
    public void save(T entity) {
        if (isNull(entity.getId())) {
            entity.setId(genereateId());
        }
        entities.put(entity.getId(), entity);
    }

    @Override
    public void delete(T entity) {
        if (isNull(entity.getId())) {
            throw new IllegalArgumentException("Can't delete a entity without id");
        }
        entities.remove(entity.getId());
    }

    @Override
    public T findById(ID id) {
        if (!entities.containsKey(id)) {
            throw new EntityNotFoundException("Can't find entity with id:" + id);
        }
        return entities.get(id);
    }


    @Override
    public List<T> findAll() {
        return entities.values().stream().collect(toList());
    }

    @Override
    public List<T> findAllByIds(List<ID> ids) {
        List<T> result = new ArrayList<>();
        ids.forEach(id -> {
            if (!entities.containsKey(id)) {
                throw new EntityNotFoundException("Can't find entity with id:" + id);
            }
            result.add(entities.get(id));
        });
        return result;
    }

    protected abstract ID genereateId();
}
