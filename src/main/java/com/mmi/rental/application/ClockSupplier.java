package com.mmi.rental.application;

import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Duration;
import java.util.Objects;
import java.util.function.Supplier;

@Component
public class ClockSupplier implements Supplier<Clock> {

    private Clock offsetClock;

    @Override
    public Clock get() {
        return Objects.nonNull(offsetClock) ? offsetClock : Clock.systemDefaultZone();
    }

    public void offsetByHours(long hours) {
        Duration offset = Duration.ofHours(hours);
        offsetClock = Clock.offset(Clock.systemDefaultZone(), offset);
    }

    public void reset() {
        offsetClock = null;
    }
}
