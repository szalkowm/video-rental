package com.mmi.rental.application.dto;


import lombok.Data;

@Data
public class Error {
    private String message;
}
