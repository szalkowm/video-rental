package com.mmi.rental.application.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ClockOffsetRequest {

    @NotNull
    private Integer offsetHours;
}
