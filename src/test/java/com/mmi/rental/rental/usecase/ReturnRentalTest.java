package com.mmi.rental.rental.usecase;

import com.mmi.rental.application.persistence.Repository;
import com.mmi.rental.rental.InvalidRentalOperationException;
import com.mmi.rental.rental.Rental;
import com.mmi.rental.rental.prices.PriceCalculator;
import com.mmi.rental.rental.prices.PriceCalculatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReturnRentalTest {

    @InjectMocks
    private ReturnRental returnRental;

    @Mock
    private Repository<Rental, String> rentalRepository;

    @Mock
    private Rental rental;

    @Mock
    private Rental.Item item;

    @Mock
    private Supplier<Clock> clockSupplier;

    @Mock
    private PriceCalculator priceCalculator;

    @Mock
    private PriceCalculatorFactory calculatorFactory;

    private final static Instant NOW = Instant.ofEpochSecond(1683650123000L);

    private final static String RENTAL_ID = "test id";

    @BeforeEach
    public void initMocks() {
        when(rentalRepository.findById(RENTAL_ID)).thenReturn(rental);
    }

    @Test
    void exceptionOnIncorrectStatus() {
        when(rental.getStatus()).thenReturn(Rental.Status.RETURNED);
        InvalidRentalOperationException exception = assertThrows(InvalidRentalOperationException.class,
                () -> returnRental.returnRental(RENTAL_ID));
        assertEquals("Can only return an active rental", exception.getMessage());
    }

    @Test
    void returnRental() {
        when(rental.getStatus()).thenReturn(Rental.Status.ACTIVE);
        when(rental.getItems()).thenReturn(List.of(item));
        when(clockSupplier.get()).thenReturn(Clock.fixed(NOW, ZoneId.of("UTC")));
        when(calculatorFactory.getCalculator(any())).thenReturn(priceCalculator);
        when(priceCalculator.calculatePrice(anyInt(), anyInt())).thenReturn(new BigDecimal("12.00"));
        when(item.getUpfrontFee()).thenReturn(new BigDecimal("7.00"));

        returnRental.returnRental(RENTAL_ID);
        verify(rental).returnRental(NOW);
        verify(item).setLateFee(new BigDecimal("5.00"));
        verify(rentalRepository).save(rental);
    }

}