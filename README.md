### Running the app

JDK of version 17 or higher is required to run the project.

You can run the app using by following command in the terminal while in the root project directory:

Windows

```shell
.\gradlew.bat bootRun
```

Linux

```shell
./gradlew bootRun
```

It is also possible to run the application by importing the project into Intellij Idea IDE and running class
```
com.mmi.rental.application.RentalApplication
```

### Endpoints 
Once the project is running. Swagger definition will become available
under [http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/).

For purpose of client generation you can also use OpenAPI definition form here [http://localhost:8080/v3/api-docs](http://localhost:8080/swagger-ui/index.html#/).

Finally, you can use import postman collection from root of the project **video_rental.postman_collection.json**

### Usage

You can **browse available** movies with movies get
endpoint [http://localhost:8080/swagger-ui/index.html#/movies-controller/allMovies](http://localhost:8080/swagger-ui/index.html#/)

You can **check the price** the cost of a rental using rental quote
post [http://localhost:8080/swagger-ui/index.html#/rentals-controller/quoteRental](http://localhost:8080/swagger-ui/index.html#/)
using similar payload.

```json
{
  "numberOfDays": "1", // a positve int representing number of days declared for the rental
  "items": [
    {
      "movieId": "spiderMan2",  // id of a movie you wish to rent. Comes from movies get endpoint
      "quantity": "1" // number of copies you wish to rent
    }
  ]
}
```

You can **create** a rental using rental POST endpoint [http://localhost:8080/swagger-ui/index.html#/rentals-controller/rent](http://localhost:8080/swagger-ui/index.html#/) It uses
the same request format as quote endpoint.

You can **return** a rental using PUT return
endpoint [http://localhost:8080/swagger-ui/index.html#/rentals-controller/returnRental](http://localhost:8080/swagger-ui/index.html#/).
You need to provide rental id as path variable. You can get the id in response of POST or GET rentals endpoints.
This endpoint will assign the late fees automatically.

To see the late fees you might need to artificially offset the app clock. This can be done 
via clock offset POST endpoint [http://localhost:8080/swagger-ui/index.html#/clock-controller/offsetClock](http://localhost:8080/swagger-ui/index.html#/)
```json
{
  "offsetHours": "480" //this will push app clokc 480 hours forward
}
```


### Notes

- authentication/authorization features were ignored
- Example unit test is present here 
    ```
  com.mmi.rental.rental.usecase.ReturnRentalTest
  ```
- I did not have enough time to add an integration test setup but that would consist of setting up a test spring context
with rest endpoints active and test the app through them. This is something that one would run on demand or by project
piplene as it would be much slower than unit test.
- I decided to use feature package structure hence three top level packages were created:
  - application - for all the code needed for the app to run and some shared code
  - inventory - holding the inventory of movies
  - rental - holding rental logic.
- The consequence of this is that putting rental price calculation in the _Movie_ class would result in a cyclic dependency.
As rental feature would need to be aware of inventory feature to work and also inventory feature would be aware of rental feature
due to price calculation. That's why I decided to put price calculation code solely in rental package
and create a factory that would provide appropriate calculator based on type of movie. This has also
and advantage in case the calculation would get sophisticated (calling 3rd party services for example)
- Usually I'd separate the app into three layers: 
  - Communication -> controllers and dtos
  - Service -> logic and use cases
  - Persistence -> repositories. 
- Doe to project simplification I did not follow this separation strictly and
  you can see a controller calling repository directly and also an endpoint returning a service level object
  instead of a dto.
- I'd seriously consider replacing enum movie type with a string hence allowing to add support for new
types by changing just the config without touching the code.
- I omitted movie deletion as it would it require some more compex logic for handling cases when deleting a movie
that is in an active rental